# Encryption PHP Class #

A secure hmac 3 part shared key encryption class. RIJNDAEL_256 AES CBC @ 1024 cost, with sha256 HMAC keys.

###Composer###

```
#!json

{
    "require": {
        "lcherone/encryption": "dev-master"
    }
}
```

### Example: ###

```
#!php
<?php
require 'vendor/autoload.php';

use Encryption\Encryption;

$crypt = new Encryption();

# Something to Encrypt
$str = "encrypt this";

# Generate a final key made up from 3 other keys - not for storing
$key = $crypt->makeKey(
    'a token key',
    'a public key',
    'a private key'
);

# Encrypt
echo $encrypted = $crypt->encrypt($str, $key);

# Decrypt
echo $crypt->decrypt($encrypted, $key);
?>
```
https://packagist.org/packages/lcherone/encryption