<?php
namespace Encryption;

class Encryption
{
    const CIPHER  = MCRYPT_RIJNDAEL_256;
    const MODE    = MCRYPT_MODE_CBC;
    const KEYHASH = 'sha256'; //  sha1 |     sha256 |     sha512
    const IVSTART = -32;    //-20 sha1 | -32 sha256 | -64 sha512
    const ROUNDS  = 1024;
    const IV_SIZE = 64;

    /**
     * Decrypt
     *
     * @param mixed $data - data to be decrypted
     * @param string $key - secret computed decryption key
     * @return string
     */
    public function decrypt($data, $key)
    {
        $salt = substr($data, 0, self::IV_SIZE);
        $enc  = substr($data, self::IV_SIZE, self::IVSTART);
        $mac  = substr($data, self::IVSTART);
        list ($cKey, $mKey, $iv) = $this->getKeys($salt, $key);
        if ($mac !== hash_hmac(self::KEYHASH, $enc, $mKey, true)) {
            return false;
        }
        $dec = mcrypt_decrypt(
            self::CIPHER,
            $cKey,
            $enc,
            self::MODE,
            $iv
        );
        return $this->unpad($dec);
    }

    /**
     * Encrypt
     *
     * @param mixed $data - data to be encrypted
     * @param string $key - secret computed encryption key
     * @return string - !you might want to base64/91 if sending over the wire!
     */
    public function encrypt($data, $key)
    {
        $salt = mcrypt_create_iv(self::IV_SIZE, MCRYPT_DEV_URANDOM);
        list ($cKey, $mKey, $iv) = $this->getKeys($salt, $key);
        $enc = mcrypt_encrypt(
            self::CIPHER,
            $cKey,
            $this->pad($data),
            self::MODE,
            $iv
        );
        return $salt . $enc . hash_hmac(self::KEYHASH, $enc, $mKey, true);
    }

    /**
     * Make Key
     *
     * @param string $token - token key
     * @param string $public - public key
     * @param string $private - private key
     * @return string
     */
    public function makeKey($token, $public, $private)
    {
        $key = hash_hmac(self::KEYHASH, $token, $public);
        $key = hash_hmac(self::KEYHASH, $key, $private);
        return $key;
    }

    /**
    * Get Key
    *
    * @param string $salt
    * @param string $key
    * @return string
    */
    protected function getKeys($salt, $key)
    {
        $ivSize  = mcrypt_get_iv_size(self::CIPHER, self::MODE);
        $keySize = mcrypt_get_key_size(self::CIPHER, self::MODE);
        $len     = 2 * $keySize + $ivSize;
        $key     = $this->pbkdf2(self::KEYHASH, $key, $salt, self::ROUNDS, $len);
        $cKey    = substr($key, 0, $keySize);
        $mKey    = substr($key, $keySize, $keySize);
        $iv      = substr($key, 2 * $keySize);

        return array($cKey, $mKey, $iv);
    }

    /**
    * PBKDF2 key derivation function
    * @return string
    */
    protected function pbkdf2($algo, $key, $salt, $rounds, $length)
    {
        $size   = strlen(hash($algo, '', true));
        $len    = ceil($length / $size);
        $result = '';
        for ($i = 1; $i <= $len; $i++) {
            $tmp = hash_hmac($algo, $salt . pack('N', $i), $key, true);
            $res = $tmp;
            for ($j = 1; $j < $rounds; $j++) {
                $tmp  = hash_hmac($algo, $tmp, $key, true);
                $res ^= $tmp;
            }
            $result .= $res;
        }
        return substr($result, 0, $length);
    }

    /**
    * Cipher Block Pad
    *
    * Pads the data length to match the encryption cipher block length.
    *
    * @param string $data
    * @return string
    */
    protected function pad($data)
    {
        $len = mcrypt_get_block_size(self::CIPHER, self::MODE);
        $padAmount = $len - strlen($data) % $len;
        if ($padAmount == 0) {
            $padAmount = $len;
        }
        return $data . str_repeat(
            chr($padAmount),
            $padAmount
        );
    }

    /**
    * Cipher Block Unpad
    *
    * UnPads the encryption cipher block length data length from the string.
    *
    * @param string $data
    * @return string
    */
    protected function unpad($data)
    {
        $len = mcrypt_get_block_size(self::CIPHER, self::MODE);
        $last = ord($data[strlen($data) - 1]);
        if ($last > $len) {
            return false;
        }
        if (substr($data, -1 * $last) !== str_repeat(chr($last), $last)) {
            return false;
        }
        return substr($data, 0, -1 * $last);
    }

}
